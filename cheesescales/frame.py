import ezdxf

DEFAULT_FRAME_WIDTH = 127
DEFAULT_FRAME_HEIGHT = 142.875
DEFAULT_FRAME_LAYER = 'FRAME_LAYER'


def get_width(pair):
    return pair[0]


def get_height(pair):
    return pair[1]


def add_points(origin, delta):
    return tuple(map(sum, zip(origin, delta)))


def drawing_layer_add_line(drawing, layer, start, end, dxfattribs={}):
    dxfattribs['layer'] = layer
    modelspace = drawing.modelspace()
    modelspace.add_line(start, end, dxfattribs=dxfattribs)
    return drawing


def drawing_layer_add_rectangle(drawing, layer, left, top, right, bottom):
    drawing_layer_add_line(drawing,
                           layer,
                           (left, top),
                           (right, top))
    drawing_layer_add_line(drawing,
                           layer,
                           (left, top),
                           (left, bottom))
    drawing_layer_add_line(drawing,
                           layer,
                           (right, bottom),
                           (left, bottom))
    drawing_layer_add_line(drawing,
                           layer,
                           (right, bottom),
                           (right, top))
    return drawing


def drawing_frame_none(drawing, origin=(0, 0), width=DEFAULT_FRAME_WIDTH, height=DEFAULT_FRAME_HEIGHT, layer=DEFAULT_FRAME_LAYER, dxfattribs=None):
    if layer in drawing.layers:
        raise ValueError('Drawing already contains a layer named {!r}.'.format(layer))
    else:
        if dxfattribs is None:
            dxfattribs = {'linetype': 'CONTINOUS', 'color': 0}

        top_left = add_points(origin,
                              (0, height))
        bottom_right = add_points(origin,
                                  (width, 0))
            
        drawing.layers.new(layer, dxfattribs=dxfattribs)
        drawing_layer_add_rectangle(drawing,
                                    layer,
                                    get_width(top_left),
                                    get_height(top_left),
                                    get_width(bottom_right),
                                    get_height(bottom_right))
    return drawing


def main():
    # or use the AutoCAD release name ezdxf.new(dxfversion='R2010')
    drawing = ezdxf.new(dxfversion='R12')
    drawing = drawing_frame_none(drawing)

    drawing.layers.new('TEXTLAYER',
                       dxfattribs={'color': 1})
    modelspace = drawing.modelspace()
    # use set_pos() for proper TEXT alignment
    # the relations between halign, valign, insert and align_point are tricky.
    modelspace.add_text('Test',
                        dxfattribs={'layer': 'TEXTLAYER',
                                    'height': 50,
                                    'width': 50}).set_pos(((DEFAULT_FRAME_WIDTH / 2) - 50,
                                                            (DEFAULT_FRAME_HEIGHT / 2) - 15),
                                                           align='CENTER')
    drawing.saveas('test.dxf')


if '__main__' == __name__:
    main()
